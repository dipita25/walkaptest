package com.walkap.walkaptest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WalkaptestApplication {

	public static void main(String[] args) {
		SpringApplication.run(WalkaptestApplication.class, args);
	}
}
