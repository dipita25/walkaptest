package com.walkap.walkaptest;

public class TestNumber {
    private int tapedNumber;

    public TestNumber(int tapedNumber){
        this.tapedNumber = tapedNumber;
    }

    /**
     * @return int return the number
     */
    public int getTapedNumber() {
        return tapedNumber;
    }

    /**
     * @param number the number to set
     */
    public void setTapedNumber(int number) {
        this.tapedNumber = number;
    }

}
