package com.walkap.walkaptest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TestController {

    @GetMapping("/")
    public String welcome(Model model) {
        model.addAttribute("result", "");
        model.addAttribute("alert", "");

        return "test";
	}

    @PostMapping("/test")
    public String test(@ModelAttribute(name = "testNumber") TestNumber testNumber, Model model) {
		
        int tapedNumber = testNumber.getTapedNumber();
        if(tapedNumber >= 1 && tapedNumber <= 20){

            if((tapedNumber % 3 == 0) && (tapedNumber % 5 == 0)){
                model.addAttribute("result", "Walkap");
            }
            else if(tapedNumber % 3 == 0){
                model.addAttribute("result", "Wal");
            }
            else if(tapedNumber % 5 == 0){
                model.addAttribute("result", "kap");
            }
            else{
                System.out.println(testNumber.getTapedNumber());
                model.addAttribute("result", tapedNumber);
            }
        }
        else{
            model.addAttribute("result", "");
            model.addAttribute("alert", "le nombre doit être compris entre 1 et 20");
        }
		return "test";
	}
}
